﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootcampCoreServices
{
    class SingletonMainListOfRequests
    {
        private static SingletonMainListOfRequests singletonMainListOfRequests;

        private List<Request> listOfRequests = new List<Request>();

        private SingletonMainListOfRequests()
        {
        }

        public static SingletonMainListOfRequests getInstance()
        {

            if (singletonMainListOfRequests == null)
            {
                singletonMainListOfRequests = new SingletonMainListOfRequests();
            }
            return singletonMainListOfRequests;
        }

        public void addRequest(Request request)
        {
            listOfRequests.Add(request);
        }

        public List<Request> getList()
        {
            return listOfRequests;
        }

        public void removeRequest(Request request)
        {
            listOfRequests.Remove(request);
        }

        public bool hasAnyRequests()
        {
            if(listOfRequests.Count >= 1)
            {
                return true;
            }
            return false;
        }

    }
}
