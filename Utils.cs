﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;

namespace BootcampCoreServices
{
    class Utils
    {

        /// <summary>
        /// Extracting data from streamReader and deseliarizing it to List of Requests
        /// </summary>
        /// <param name="streamReader">streamReader of selected File</param>
        /// <param name="fileExtension">e.x. .csv or .xml etc.</param>
        /// <returns></returns>
        public static Tuple<List<Request>, String> fileReader(StreamReader streamReader, string fileExtension)
        {
            List<Request> orders = new List<Request>();

            //Removing '.' character from extension
            fileExtension = fileExtension.Substring(1);
            string ErrorOutput = "";

            //Line Counter value
            int i = 1;

            switch (fileExtension)
            {
                case "csv":
                    //Reading first line to remove HEADER
                    streamReader.ReadLine();

                    while (!streamReader.EndOfStream)
                    {
                        string lineFromTheFile = streamReader.ReadLine();
                        string[] OrderItem = lineFromTheFile.Split(',');

                        if (OrderItem.Length == 5)
                        {
                            //Validation of provided data
                            string currentErrorOutput = parseErrorsObjectsLAZY(OrderItem[0], OrderItem[1], OrderItem[2], OrderItem[3], OrderItem[4]);

                            //if there is no Error text then process and create new Request
                            if (String.IsNullOrEmpty(currentErrorOutput))
                            {
                                orders.Add(new Request(OrderItem[0], Convert.ToInt64(OrderItem[1]), OrderItem[2], Convert.ToInt16(OrderItem[3]), double.Parse(OrderItem[4], CultureInfo.InvariantCulture)));
                            }
                            else
                            {
                                ErrorOutput += "Request number " + i + " is invalid -  " + currentErrorOutput + "\r\n";
                            }
                        }
                        else
                        {
                            ErrorOutput += "invalid file ";
                            break;
                        }

                        //line counter incrementation. Required only for error parsing number 
                        i++;
                    }
                    return Tuple.Create(orders, ErrorOutput);

                case "json":
                    string content = streamReader.ReadToEnd();
                    try
                    {
                        RootObject requests = JsonConvert.DeserializeObject<RootObject>(content);
                        orders = requests.requests;

                        orders.RemoveAll(o => parseERRORSObjectNEW(o, ref ErrorOutput, ref i));
                    }
                    catch(Exception)
                    {
                       // ErrorOutput += "JSON: error with deserializing object;" + ex.Message;
                        ErrorOutput += "Invalid JSON file";
                    }

                    return Tuple.Create(orders, ErrorOutput);

                case "xml":

                    XmlDocument requestsXML = new XmlDocument();
                    try
                    {
                        //try parse XML file - 
                        //if its strucure is not correct throw ERROR and stop
                        requestsXML.Load(streamReader);
                    }
                    catch (Exception ex)
                    {
                        ErrorOutput += "invalid file -  " + ex.Message;
                        break;
                    }
                    foreach (XmlNode node in requestsXML.DocumentElement)
                    {
                        var nodeItem = node.ChildNodes;

                        //TODO - nie sprawdzam nazwy albo inna kolejnosc
                        //Validation of provided data 
                        String currentErrorsXML = parseErrorsObjectsLAZY(nodeItem[0].InnerText, nodeItem[1].InnerText, nodeItem[2].InnerText, nodeItem[3].InnerText, nodeItem[4].InnerText);

                        //if there is no Error text then process and create new Request
                        if (String.IsNullOrEmpty(currentErrorsXML))
                        {
                            orders.Add(new Request(nodeItem[0].InnerText, Convert.ToInt64(nodeItem[1].InnerText), nodeItem[2].InnerText, Convert.ToInt16(nodeItem[3].InnerText), double.Parse(nodeItem[4].InnerText, CultureInfo.InvariantCulture.NumberFormat)));
                        }
                        else
                        {
                            ErrorOutput += "Request number " + i + " is invalid -  " + currentErrorsXML + "\r\n";
                        }
                        i++;
                    }
                    return Tuple.Create(orders, ErrorOutput);
            }
            return Tuple.Create(orders, ErrorOutput);
        }

        /// <summary>
        /// Provide simple report 
        /// </summary>
        /// <param name="orders"></param>
        /// <param name="reportName"></param>
        /// <param name="clientNumber"></param>
        /// <param name="pricefrom"></param>
        /// <param name="priceto"></param>
        /// <returns></returns>
        public static System.Data.DataTable provideSingleValueReport(List<Request> orders, string reportName, string clientNumber = "", double pricefrom = 10.00, double priceto = 15.00)
        {
            var dictionary = new Dictionary<string, List<string>>();
            var dataTable = new DataTable();

            switch (reportName)
            {
                case "Ilość zamówień":
                    #region Ilość zamówień
                    HashSet<long?> numbRequests = new HashSet<long?>();
                    List<string> rows = new List<string>();

                    //todo Tutaj NULL JEST - ZABEZPIECZYC
                    foreach (var request in orders)
                    {
                        numbRequests.Add(request.Request_id);
                    }

                    //Add ROWS and Columns in to dictionary
                    dictionary.Add("Number of Requests", rows);
                    rows.Add(Convert.ToString(numbRequests.Count));


                    break;
                #endregion
                case "Ilość zamówień dla klienta o wskazanym identyfikatorze":
                    #region Ilość zamówień dla klienta o wskazanym identyfikatorze
                    //TODO - Wyjasnic ILOSC REQUESTOW klienta CZY ILOSC WSZYSTKICH ZAMOWIEN KLIENTA??

                    int clientOrdersNumber;
                    List<string> clientNumberList = new List<string>();
                    List<string> clientOrdersList = new List<string>();

                    if (!String.IsNullOrEmpty(clientNumber))
                    {
                        //get all requests of specific client
                        var onlyClientRequests =  orders.Where(x => x.Client_Id.Equals(clientNumber));
                        HashSet<long?> ClientOrdersSet = new HashSet<long?>(); 
                        foreach (Request request in onlyClientRequests)
                        {
                            ClientOrdersSet.Add(request.Request_id);
                        }
                        clientOrdersNumber = ClientOrdersSet.Count;
                       // clientOrdersNumber = orders.Count(c => c.Client_Id.Equals(clientNumber));

                        //adding Client Number Column and 1st Row
                        dictionary.Add("Client Number", clientNumberList);
                        clientNumberList.Add(clientNumber);

                        //adding Order Number Column and 1st Row
                        dictionary.Add("Amount of requests", clientOrdersList);
                        clientOrdersList.Add(Convert.ToString(clientOrdersNumber));

                        break;
                    }

                    break;
                #endregion
                case "Łączna kwota zamówień":
                    #region Łączna kwota zamówień
                    double? allOrdersPrice = 0.00;
                    List<string> rowAmountOrders = new List<string>();

                    foreach (var request in orders)
                    {
                        allOrdersPrice += request.Price;
                    }

                    //Add ROWS and Columns in to dictionary
                    rowAmountOrders.Add(Convert.ToString(allOrdersPrice));
                    dictionary.Add("Total Amount of Orders", rowAmountOrders);
                    

                    break;
                #endregion
                case "Łączna kwota zamówień dla klienta o wskazanym identyfikatorze":
                    #region Łączna kwota zamówień dla klienta o wskazanym identyfikatorze
                    double? OrdersPriceOneClient = 0.00;
                    List<string> rowAmountOrdersPerClient = new List<string>();

                    foreach (var request in orders)
                    {
                        if (request.Client_Id.Equals(clientNumber))
                        {
                            OrdersPriceOneClient += request.Price;
                        }
                    }

                    //Add ROWS and Columns in to dictionary
                    rowAmountOrdersPerClient.Add(Convert.ToString(OrdersPriceOneClient));
                    dictionary.Add("Total Amount of Orders", rowAmountOrdersPerClient);

                    break;
                #endregion
                case "Lista zamówień dla klienta o wskazanym identyfikatorze":
                    #region Lista zamówień dla klienta o wskazanym identyfikatorze
                    DataTable dt = new DataTable();
                    var onlyClientRequestsList = orders.Where(x => x.Client_Id.Equals(clientNumber));

                    dt.Columns.Add("Client_Id");
                    dt.Columns.Add("Request_id");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Price");

                    for (int i = 0; i < onlyClientRequestsList.Count(); i++)
                    {
                        var row = dt.Rows.Add();

                        row["Client_Id"] = onlyClientRequestsList.ElementAt(i).Client_Id;
                        row["Request_id"] = onlyClientRequestsList.ElementAt(i).Request_id;
                        row["Name"] = onlyClientRequestsList.ElementAt(i).Name;
                        row["Quantity"] = onlyClientRequestsList.ElementAt(i).Quantity;
                        row["Price"] = onlyClientRequestsList.ElementAt(i).Price;

                    }

                    return dt;
                #endregion
                case "Średnia wartość zamówienia":
                    #region Średnia wartość zamówienia
                    
                    List<string> clientNumberListAVG = new List<string>();
                    List<string> clientRequestListAVG = new List<string>();
                    List<string> clientPriceAVG = new List<string>();

                    //list of unique clients
                    var ClientNumbers = orders.Select(x => x.Client_Id).Distinct();

                    foreach (string oneCleintNumber in ClientNumbers)
                    {
                        //extract all requests from specific client
                        var allUniqueRequestsIdsAVG = orders.Where(x => x.Client_Id.Equals(oneCleintNumber)).Select(x => x.Request_id).Distinct();

                        foreach (var oneRequestNumber in allUniqueRequestsIdsAVG)
                        {
                            //add client number to List
                            clientNumberListAVG.Add(oneCleintNumber);
                            //add client request to List
                            clientRequestListAVG.Add(Convert.ToString(oneRequestNumber));
                            //calculate avarage of price per Request nad Client
                            clientPriceAVG.Add(orders.Where(x => x.Client_Id.Equals(oneCleintNumber) && x.Request_id.Equals(oneRequestNumber)).Average(x => x.Price).ToString());
                        }
                    }
                                    

                    //Add Client Number Column and 1st Row
                    dictionary.Add("Client_Id", clientNumberListAVG);

                    dictionary.Add("Request_id", clientRequestListAVG);

                    dictionary.Add("Price_AVG", clientPriceAVG);


                    break;
                    #endregion
                case "Średnia wartość zamówienia dla klienta o wskazanym identyfikatorze":
                    #region Średnia wartość zamówienia dla klienta o wskazanym identyfikatorze

                    List<string> clientNumberListAVGbyClient_Id = new List<string>();
                    List<string> clientRequestListAVGbyClient_Id = new List<string>();
                    List<string> clientPriceAVGbyClient_Id = new List<string>();

                        //extract all requests from specific client
                        var allUniqueRequestsIds = orders.Where(x => x.Client_Id.Equals(clientNumber)).Select(x => x.Request_id).Distinct();

                        foreach (var oneRequestNumber in allUniqueRequestsIds)
                        {
                            //add client number to List
                            clientNumberListAVGbyClient_Id.Add(clientNumber);
                            //add client request to List
                            clientRequestListAVGbyClient_Id.Add(Convert.ToString(oneRequestNumber));
                            //calculate avarage of price per Request nad Client
                            clientPriceAVGbyClient_Id.Add(orders.Where(x => x.Client_Id.Equals(clientNumber) && x.Request_id.Equals(oneRequestNumber)).Average(x => x.Price).ToString());
                        }
                    

                    //Add Client Number Column and 1st Row
                    dictionary.Add("Client_Id", clientNumberListAVGbyClient_Id);

                    dictionary.Add("Request_id", clientRequestListAVGbyClient_Id);

                    dictionary.Add("Price_AVG", clientPriceAVGbyClient_Id);


                    break;
                #endregion
                case "Ilość zamówień pogrupowanych po nazwie":
                    #region Ilość zamówień pogrupowanych po nazwie
                    List<string> RequestName = new List<string>();
                    List<string> numberofRequestsByName = new List<string>();

                    
                    var uniqueNames = orders.Select(x => x.Name).Distinct();

                    foreach (string itemName in uniqueNames)
                    {
                        RequestName.Add(itemName);
                        numberofRequestsByName.Add(orders.Count(x => x.Name.Equals(itemName)).ToString());
                    }


                    ////adding Client Number Column and 1st Row
                    dictionary.Add("Name", RequestName);

                    ////adding Order Number Column and 1st Row
                    dictionary.Add("Amount of requests", numberofRequestsByName);

                    //output Name | number of Requests
                    break;
                #endregion
                case "Ilość zamówień pogrupowanych po nazwie dla klienta o wskazanym identyfikatorze":
                    #region Ilość zamówień pogrupowanych po nazwie dla klienta o wskazanym identyfikatorze
                    List<string> Client_Ids = new List<string>();
                    List<string> RequestNameById = new List<string>();
                    List<string> numberofRequestsByNameById = new List<string>();


                    var uniqueRequestNames = orders.Where(x => x.Client_Id.Equals(clientNumber)).Select(x => x.Name).Distinct();

                    foreach (string itemName in uniqueRequestNames)
                    {
                        Client_Ids.Add(clientNumber);
                        RequestNameById.Add(itemName);
                        numberofRequestsByNameById.Add(orders.Count(x => x.Name.Equals(itemName) && x.Client_Id.Equals(clientNumber)).ToString());
                    }

                    dictionary.Add("Client_Id", Client_Ids);

                    ////adding Request Name Column and 1st Row
                    dictionary.Add("Name", RequestNameById);

                    ////adding Order Number Column and 1st Row
                    dictionary.Add("Amount of requests", numberofRequestsByNameById);

                    //output Name | number of Requests
                    break;
                #endregion
                case "Zamówienia w podanym przedziale cenowym":
                    #region Zamówienia w podanym przedziale cenowym

                    DataTable dataTable_ordersInRange = new DataTable();
                    if (pricefrom <= priceto)
                    {

                        var ordersByInterval = orders.Where(x => x.Price >= pricefrom && x.Price <= priceto);

                        dataTable_ordersInRange.Columns.Add("Client_Id");
                        dataTable_ordersInRange.Columns.Add("Request_id");
                        dataTable_ordersInRange.Columns.Add("Name");
                        dataTable_ordersInRange.Columns.Add("Quantity");
                        dataTable_ordersInRange.Columns.Add("Price");

                        for (int i = 0; i < ordersByInterval.Count(); i++)
                        {
                            var row = dataTable_ordersInRange.Rows.Add();

                            row["Client_Id"] = ordersByInterval.ElementAt(i).Client_Id;
                            row["Request_id"] = ordersByInterval.ElementAt(i).Request_id;
                            row["Name"] = ordersByInterval.ElementAt(i).Name;
                            row["Quantity"] = ordersByInterval.ElementAt(i).Quantity;
                            row["Price"] = ordersByInterval.ElementAt(i).Price;

                        }
                    }
                    return dataTable_ordersInRange;
                    #endregion
            }
            #region changing DICTIONARY in to DATA TABLE 
            //calculate number of rows 
            int NumberOfRows = dictionary.Cast<KeyValuePair<string, List<string>>>().Select(x => x.Value.Count).Max();

            foreach (var key in dictionary.Keys)
            {
                dataTable.Columns.Add(key);
            }

            //add Rows and Columns in to dataTable
            for (int i = 0; i < NumberOfRows; i++)
            {
                var row = dataTable.Rows.Add();
                foreach (var key in dictionary.Keys)
                {
                    try
                    {
                        row[key] = dictionary[key][i];
                    }
                    catch
                    {
                        row[key] = null;
                    }
                }
            }
            #endregion

            return dataTable;

        }

        /// <summary>
        /// Validate XML and CSV elements correrectnes 
        /// </summary>
        /// <param name="firstElement">Client_Id</param>
        /// <param name="secondElement">Request_id</param>
        /// <param name="thirdElement">Name</param>
        /// <param name="fourthElement">Quantity</param>
        /// <param name="fifthElement">Price</param>
        /// <returns>strings with errors separated by \r\n -> i.e. xxxx\r\n xxxxxx\r\n xxx..</returns>
        private static string parseErrorsObjectsLAZY(string firstElement, string secondElement, string thirdElement, string fourthElement, string fifthElement)
        {
            //list of errors separated by ';' char
            String listOfErrors = "";

            #region ClientId Element validation
            if (!String.IsNullOrEmpty(firstElement))
            {
                if (firstElement.Contains(' '))
                {
                    listOfErrors += "ClientId include space;";
                }
                if (firstElement.Length > 6)
                {
                    listOfErrors += "ClientId is longer than 6 characters;";
                }
            }
            else
            {
                listOfErrors += "ClientId was not provied;";
            }
            #endregion

            #region RequestId Element validation
            if (!String.IsNullOrEmpty(secondElement))
            {
                //checking if provided item is number
                try
                {
                    Convert.ToInt64(secondElement);
                }
                catch (Exception ex)
                {
                    listOfErrors += "RequestId is not a valid number;";
                }
            }
            else
            {
                listOfErrors += "RequestId was not provied;";
            }
            #endregion

            #region Name Element validation
            if (!String.IsNullOrEmpty(thirdElement))
            {
                //checking if provided item is number
                if (thirdElement.Length > 255)
                {
                    listOfErrors += " Name contains too many characters (up to 255) (inclusive);";
                }
            }
            else
            {
                listOfErrors += "Name was not provied;";
            }
            #endregion

            #region Quantity Element validation
            if (!String.IsNullOrEmpty(fourthElement))
            {
                //checking if provided item is number
                try
                {
                    Convert.ToInt16(fourthElement);
                }
                catch (Exception)
                {
                    listOfErrors += "Quantity is not a valid number;";
                }
            }
            else
            {
                listOfErrors += "Quantity was not provied;";
            }
            #endregion

            #region Price Element validation
            if (!String.IsNullOrEmpty(fifthElement))
            {
                //checking if provided item is number
                try
                {
                    double.Parse(fifthElement, CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    listOfErrors += "Price is not a valid number (i.e. 10.00);";
                }
            }
            else
            {
                listOfErrors += "Price was not provied;";
            }
            #endregion


            return listOfErrors;

        }

        /// <summary>
        /// Validation for JSON
        /// </summary>
        /// <param name="request"></param>
        /// <param name="ErrorString"></param>
        /// <returns></returns>
        private static bool parseERRORSObjectNEW(Request request,ref string ErrorString, ref int requestNumber)
        {
            string client_Id = request.Client_Id;
            long? request_id = request.Request_id;
            string name = request.Name;
            int? quantity = request.Quantity;
            double? price = request.Price;

            //list of errors separated by ';' char
            string listOfErrorsX = "";
            string prefix = "Request Number " + requestNumber;
            #region ClientId Element validation
            if (!String.IsNullOrEmpty(client_Id))
            {
                if (client_Id.Contains(' '))
                {
                    listOfErrorsX += "ClientId include space;\r\n";
                }
                if (client_Id.Length > 6)
                {
                    listOfErrorsX += "ClientId is longer than 6 characters;\r\n";
                }
            }
            else
            {
                listOfErrorsX += "ClientId was not provied;\r\n";
            }
            #endregion

            #region RequestId Element validation
            if (request_id.HasValue)
            {
                if (quantity == -1)
                {
                    listOfErrorsX += "RequestId is not a valid number;\r\n";
                }
            }
            else
            {
                listOfErrorsX += "RequestId was not provied;\r\n";
            }
            #endregion

            #region Name Element validation
            if (!String.IsNullOrEmpty(name))
            {
                //checking if provided item is number
                if (name.Length > 255)
                {
                    listOfErrorsX += " Name contains too many characters (up to 255) (inclusive);\r\n";
                }
            }
            else
            {
                listOfErrorsX += "Name was not provied;\r\n";
            }
            #endregion

            #region Quantity Element validation
            if (quantity.HasValue)
            {
                if (quantity == -1)
                {
                    listOfErrorsX += "Quantity is not a valid number;\r\n";
                }
            }
            else
            {
                listOfErrorsX += "Quantity was not provied;\r\n";
            }
            #endregion

            #region Price Element validation
            if (price.HasValue)
            {
                if (price == -1 || price == 0)
                {
                    listOfErrorsX += "Price is not a valid number;\r\n";
                }
            }
            else
            {
                listOfErrorsX += "Price was not provied;\r\n";
            }
            #endregion

            requestNumber++;

            if (!String.IsNullOrEmpty(listOfErrorsX))
            {
                ErrorString += prefix + "\r\n" + listOfErrorsX + "\r\n";
                return true;
            }

            return false;

        }

    }
}
